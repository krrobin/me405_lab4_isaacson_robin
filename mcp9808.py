"""
@file mcp9808.py
@author Josephine Isaacson
@author Keanau Robin
@date February 2021

@brief This file contains the MCP9808 class which acts as the driver for lab00x04. 
@details The MCP9808 driver includes the following methods:
    - init: This method initializes the driver and takes the I2C and bus address as inputs
    - check: This method verifies that the bus address given matches the bus address for the sensor
    - Celsius: This method returns the temperature value in degrees celsius
    - Fahrenheit: Returns the temperature in degrees F
    .
"""


class MCP9808:
    '''
    @brief      MCP9808 Temperature Sensor class that allows the user to gather ambient temperature data from the sensor
    @details    This class defines all the methods required for the user to properly gather data from the sensor.
    '''
    
    def __init__(self, I2C, address):
        '''
        @brief Creates an MCP9808 class
        '''
        
        ## An object copy of the I2C
        self.i2c = I2C
        
        ## An object copy of the address of the given sensor
        self.address = address
              
           

    def check(self):
        '''
        @brief      Verifies that the sensor is attached at the given bus address
        @details    This method verifies that the sensor is attached at the given bus address by checking that the value in the manufacturer ID register is correct.
        '''
        
        # Checks that the given address is correct for the sensor
        return self.i2c.is_ready(self.address)
    
    def celsius(self):
        '''
        @brief      Returns the measured ambient temperature in degrees Celsius
        @details    This method uses binary math and the information presented on the MCP9808 documentation page 25 to calculate the ambient temperature of the sensor in degrees Celsius.
        '''
        
        
        ## The byte string that is returned when we read from the sensor
        self.asciival = self.i2c.mem_read(2, addr = 24, memaddr = 5)
        
       
        ## The upper byte which holds information about the temperature above 2^3 deg C and the sign of the Temperature
        byte1 = self.asciival[0]
        
        ## The lower byte containing the temperature data for below 2^3 deg C and the 1/16ths of a degree
        byte2 = self.asciival[1]
        
        ##This defines the upper byte as the binary string that it corresponds to without the '0b' prefix
        self.upperbyte = bin(byte1)[2:]
        
        ## The lower byte as the binary string that it corresponds to, without the '0b' prefix
        self.lowerbyte = bin(byte2)[2:]
        
        # Check the sign of the temperature by checking if the 4th bit is True, if it is: the temperature is negative
        if int(self.upperbyte, 2) & int('00010000', 2):
            ## New Upper is the upper byte shifted 4 times to the right to get rid of the bits that we do not need
            NewUpper = self.upperbyte >> 4
            
            ## New Lower is the lower byte shifted 4 times to the right to be able to combine the upper and lower bytes
            NewLower = self.lowerbyte >> 4
            
            ## The temperature is 256 minus the combination of the upper and lower bytes
            
            Temperature = (256 - float(NewUpper + NewLower))
             #The 256 changes the sign
             
        # If the temperature is positive:    
        else:
            
            NewUpper = int(self.upperbyte, 2) >> 4
            
            NewLower = int(self.lowerbyte, 2) >> 4
            
            ## The temperature here is just the upper and lower bytes combined
            Temperature = ( float(NewUpper + NewLower))
            
        # Returning the temperature allows the main file to append the value    
        return(Temperature)
        
        
    def fahrenheit(self):
        '''
        @brief      Returns the measured ambient temperature in degrees Fahrenheit
        @details    This method calls the above method: Celsius, and then performs a conversion to get from the Celsius value to the Fahrenheit value.
        '''
        ## The temperature calculated in degrees Celsius
        tempc = self.celsius()
        
        ## The temperature converted from Celsius to Fahrenheit
        Temperature = tempc* (9/5) +32
        
        return(Temperature)
        

# This code is for testing purposes
from pyb import I2C
if __name__ == "__main__":
    ## the I2C object
    i2c = I2C(1, I2C.MASTER)
    ## the bus address
    address = 24
    ## The mcp class with the given inputs
    mcp = MCP9808(i2c, address)
    # Verifies that the sensor is attached at the given bus address
    mcp.check()


    
    
    


    
    
  
    