##  @file lab4main.py
#   It is important to note that Josephine Isaacson and Keanau Robin were partners for this lab.
#   
#   Documentation for lab4main.py file, which acts as the data collector for Lab 4. Essentially, this main file measures the ambient temperature through the MCP9808 temperature sensor using an I2C 
#   interface to communicate with a microcontroller. Along with that, the internal temperature of the microcontroller in the Nucleo is also read. For both sets of data, each data point is measured 
#   every minute for eight hours straight. Once data collection is complete, it is saved in a CSV file labeled "me405lab4.csv". In that CSV file, you will find the timestamp of each data point in 
#   column 1, the ambient temperature in column 2, and the internal MCU temperature in column 3. All temperature values are in celsius for the CSV file. Note that there will be an error prompt that 
#   occurs if the manufacturer ID is incorrect.
#   
#
#   File can be found here: 
#   
#   @author Keanau Robin
#   @author Josephine Isaacson
#
#   @date February 09, 2021



import pyb, utime
import array
from mcp9808 import MCP9808
from pyb import I2C



## creates and initializes the Nucleo as a master within the I2C interface
i2c = I2C(1, I2C.MASTER)

## Address of our bus
address = 24

## initializes objects for mcp9808 driver using the i2c object and specified address
mcp = MCP9808(i2c, address)

## Creates array for Ambient temperatures that will later be filled with float values
ambient = array.array('f')

## Creates array for MCU internal temperature that will later be filled with float values
stm_list = array.array('f')

## Starting time of the data collection that begins once main file is ran
start_time = utime.ticks_ms()

## variable representing the ADCAll object
adc1 = pyb.ADCAll(12, 0x70000)

## Initializes the reference voltage of adc1
mcu_vref = adc1.read_core_vref()

# Uses check() function from mcp9808 driver to check for valid manufacturer ID
mcp.check()

# If manufacturer ID is valid, then the code is ran
if mcp.check() == True: 
    
    # creates and opens a csv file that will hold the temperature data
    with open ("me405lab4.csv", "w") as me405lab4:
        # this for loop, along with the sleep command will make sure we get data each minute for 8 hours
        for n in range(480):
            curr_time = utime.ticks_ms()
            next_time = utime.ticks_diff(curr_time, start_time)/1e3
            
            STM_Temp_degC = adc1.read_core_temp()
            
            Ambient_degC = mcp.celsius()
            
            # Prints time, mcu temp, and ambient temp on putty terminal
            print('Time [s]: ' + str(next_time))
            print('STM Temp [C]: ' + str(STM_Temp_degC))
            print('Ambient Temp [C]: ' + str(Ambient_degC))
            
            # rewrites the csv files to display the time, ambient, and MCU core temp
            me405lab4.write ('{:}, {:}, {:}\n'.format(next_time, Ambient_degC, STM_Temp_degC))
            
            # delays the code for a minute before iterating again
            utime.sleep(60)
    print ("The file has by now automatically been closed.")

else:
    # an error message will pop up if the manufacturer ID is not valid
    print('Bus address is not correct. MCP9808 sensor not found.')
