
"""
@file lab00x04.py
@author: josephine
"""

# This file will be called main.py when it is loaded to the nucleo
# this will take a temp reading every 60s from the mcp9808 by interacting with the class created in mcp9808.py
# the data will be saved to a csv file on the microcontroller using the context manager
# the data columns should be the following:
#     time
#     STM32 temp
#     ambient temp (mcp9808 temp)
    
# Data will be taken until the user presses cntl c
# the file should cleanly exit without errors
# to wait between data collection use utime.sleep()
# since this wont be accurate must use utime.ticks_ms to measure time elapsed
# watch for over flow
# Data will be taken for 8 hours

